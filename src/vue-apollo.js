/* eslint-disable */
import Vue from "vue";
import VueApollo from "vue-apollo";
import { ApolloClient, InMemoryCache } from "@apollo/client";
import typeDefs from "./graphql/typedefs.gql";
import TodoFragment from "./graphql/todo.fragment.gql";
import UserFragment from "./graphql/user.fragment.gql";

const fetchUsersPendingRequests = {};
function fetchUser(id) {
  if (!fetchUsersPendingRequests[id]) {
    fetchUsersPendingRequests[id] = new Promise((ok) =>
      setTimeout(ok, Math.random() * 10000)
    )
      .then(() => fetch(`https://jsonplaceholder.typicode.com/users/${id}`))
      .then((r) => r.json())
      .finally(() => {
        delete fetchUsersPendingRequests[id];
      });
  }

  return fetchUsersPendingRequests[id];
}

const resolvers = {
  Query: {
    async todo(_, vars, { cache }) {
      const existingTodo = cache.readFragment({
        fragment: TodoFragment,
        id: `ClientTodo:${vars.id}`,
      });

      if (existingTodo) {
        return existingTodo;
      }

      const { userId, ...rawTodoData } = await fetch(
        `https://jsonplaceholder.typicode.com/todos/${vars.id}`
      ).then((r) => r.json());

      return {
        __typename: "ClientTodo",
        ...rawTodoData,
        user: { id: userId },
      };
    },

    async todos() {
      const rawTodosData = await fetch(
        "https://jsonplaceholder.typicode.com/todos"
      ).then((r) => r.json());

      return rawTodosData.map(({ userId, ...todo }) => ({
        __typename: "ClientTodo",
        ...todo,
        user: { id: userId },
      }));
    },

    async users() {
      const rawUserData = await fetch(
        "https://jsonplaceholder.typicode.com/users"
      ).then((r) => r.json());

      return rawUserData.map((user) => ({
        __typename: "ClientUser",
        ...user,
      }));
    },
  },

  ClientTodo: {
    async user(parent, _, { client }) {
      console.log("resolving", parent.user.id);
      fetchUser(parent.user.id).then(async (rawUserData) => {
        // Это не сработает!

        // const currentUser = client.readFragment(
        //   {
        //     fragment: UserFragment,
        //     id: `ClientUser:${parent.user.id}`,
        //   },
        //   true
        // );

        const currentUser = client.cache.data.lookup(
          `ClientUser:${parent.user.id}`
        );

        client.writeFragment({
          id: `ClientUser:${parent.user.id}`,
          fragment: UserFragment,
          data: {
            ...currentUser,
            ...rawUserData,
          },
        });
      });

      return {
        __typename: "ClientUser",
        id: parent.user.id,
      };
    },
  },

  ClientUser: {
    address(parent) {
      return {
        __typename: "ClientAddress",
        id: parent.id,
        ...parent.address,
      };
    },
  },
};

const apolloClient = new ApolloClient({
  cache: new InMemoryCache({
    freezeResults: true,
  }),
  resolvers,
  typeDefs,
});

Vue.use(VueApollo);

export function createProvider() {
  const apolloProvider = new VueApollo({
    defaultClient: apolloClient,
    assumeImmutableResults: true,
  });

  return apolloProvider;
}
